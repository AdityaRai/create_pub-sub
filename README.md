
Run the following command to create a topic called myTopic:----------->>>>

gcloud pubsub topics create myTopic

To see the three topics you just created, run the following command:--------->>>>>

gcloud pubsub topics list

Run the command to delete the topics---->>>>>>>>

gcloud pubsub topics delete Test1

Crete subscriptions--------->>>>>

gcloud  pubsub subscriptions create --topic myTopic mySubscription

List the subscriptions----------->>

gcloud pubsub topics list-subscriptions myTopic

Delete the subscriptions------>>>>

gcloud pubsub subscriptions delete Test1

publish the message------------>>>>>>

gcloud pubsub topics publish myTopic --message "Hello"

pull all message--------->>>>>>

gcloud pubsub subscriptions pull mySubscription --auto-ack --limit=3

